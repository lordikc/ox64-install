# Introduction

In this guide we will see how to install Linux on the Ox64 using a Raspberry Pico as a UART communication device.

The installation process consists of the following steps:

- Compilation of images.
- Preparing the Raspberry Pico for flashing the Ox64 and accessing the console.
- Connecting the Ox64 to the Raspberry Pico for flashing.
- The flashing process by itself
- Creation of the SD card with the root filesystem
- Connecting the Ox64 to the Raspberry Pico for the boot
- Booting the Ox64 Linux system

Most of the resources and documentation can be found on the [Pine64 homepage](https://wiki.pine64.org/wiki/Ox64).

# Compilation of images

The source code can be found here: [openbouffalo](https://github.com/openbouffalo/buildroot_bouffalo).

When building, you can choose between a full configuration for Ox64 (pine64_ox64_full_defconfig) or the default one (pine64_ox64_defconfig). 
The compilation takes a long long time.

```
mkdir buildroot_bouffalo && cd buildroot_bouffalo
git clone https://github.com/buildroot/buildroot
git clone https://github.com/openbouffalo/buildroot_bouffalo
export BR_BOUFFALO_OVERLAY_PATH=$(pwd)/buildroot_bouffalo
cd buildroot
#make BR2_EXTERNAL=$BR_BOUFFALO_OVERLAY_PATH pine64_ox64_defconfig
make BR2_EXTERNAL=$BR_BOUFFALO_OVERLAY_PATH pine64_ox64_full_defconfig
make
```

Once the compilation is over, the resulting files are in buildroot_bouffalo/buildroot/output/images and are:

- m0_lowload_bl808_m0.bin
- d0_lowload_bl808_d0.bin
- bl808-firmware.bin
- sdcard.img

The first three will be flashed to Ox64 the last one is the root filesystem of the SD card.

# Preparing the Raspberry Pico

The Raspberry Pico is used as a serial communication mean for flashing the files and acccessing the console.
Download the firmware from this link: [picoprobe.uf2](https://github.com/sanjay900/ox64-uart/releases/tag/v1.1)

Connect your pico to USB while pressing the *BOOT* switch. The pico appears as a mass storage device. Copy the picoprobe.uf2 file to the pico and it will disconnect automatically. The pico is ready to act as a serial communication interface.

# Connection for flashing

![Ox64 Pinout](0x64pinout.png)
![PICO Pinout](picopinout.png)

Connect:

- PICO VBUS (40) to Ox64 VBUS (40)
- PICO Ground (38) to Ox64 Ground (38)
- PICO UART1 TX/GP4 (6) to Ox64 UART0 RX/GPIO 15 (2)
- PICO UART1 RX/GP5 (7) to Ox64 UART0 TX/GPIO 14 (1)
- PICO USB to computer USB.

![Connection for flash](flash.jpg)

With a brand new Ox64 SBC you should see the Ping/Pong thing when connecting with minicom.

```
minicom -b 2000000 -D /dev/ttyACM0
```

![Ping! Pong!](pingpong.jpg)

# Flashing the firmware

Download the [Bouffalo Lab Dev Cube](https://dev.bouffalolab.com/download) application. It is meant to be used on Ubuntu but worked out of the box on Fedora 36.

Start the Ox64 in flash mode:

- *Do not plug VBUS yet*
- Hold the BOOT switch
- Plug VBUS
- Wait a bit
- Release BOOT switch

Run BLDevCube-ubuntu application and select BL808 board.

Select UART /dev/ttyACM0 and baudrate 2000000.

From the MCU tab:

- M0: set group0, Image Addr [0x58000000] and select m0_lowload_bl808_m0.bin compiled file
- D0: set group0, Image Addr [0x58100000] and select d0_lowload_bl808_d0.bin compiled file
- Click 'Create & Download'

From the IOT tab:

- Check the 'Single download' box
- Set address 0x800000
- Select bl808-firmware.bin compiled file
- Click 'Create & Download'

The flashing is done !

# Creating the SD Card

Connect you SD Card to your computer and check *carefully* its device name (dmesg or cat /proc/partitions) let say /dev/sdX (without partition nulber)
Write the image to the SD Card:
```
sudo dd if=sdcard.img of=/dev/sdX bs=8M status=progress
```

Plug the SD Card in the Ox64 and we are ready for our almost ready for the first boot.

# Connecting for the boot

Disconnect:

- PICO VBUS (40) to Ox64 VBUS (40)

Connect:

- PICO Ground (38) to Ox64 Ground (38)
- PICO UART1 TX/GP4 (6) to Ox64 GPIO 17 (31)
- PICO UART1 RX/GP5 (7) to Ox64 GPIO 16 (32)
- PICO USB to computer USB.

![Connection for boot](boot.jpg)

Now we are ready !!

# Our first boot

Before connecting VBUS, we need to setup the access to the Ox64 console.
The console will be /dev/ttyACM0 and it can be accessed through minicom:

```
minicom -b 2000000 -D /dev/ttyACM0
```

Connect VBUS et voilà !

Log as root without password.

![First boot](firstboot.png)
